import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import BooksPage from "../pages/booksPage/booksPage";
import LoginPage from "../pages/loginPage/loginPage";

const Router = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Navigate to="/login" />} />
        <Route path="/login" element={<LoginPage />} />
        <Route path="/dashboard" element={<BooksPage />} />
      </Routes>
    </BrowserRouter>
  );
};

export default Router;
