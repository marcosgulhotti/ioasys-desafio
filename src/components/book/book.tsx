import { Book } from "../../types";
import {
  StyledBackgroundCard,
  StyledInfosDiv,
  StyledSubInfos,
  StyledTitle,
} from "./style";
import defaultBook from "../../assets/books/defaultBook.png";
import { useState } from "react";
import BookModal from "../bookModal/bookModal";
import Modal from "react-modal";

const customStyles = {
  content: {
    width: "100vw",
    height: "100vh",
    background: "rgba(0, 0, 0, 0.4)",
    margin: 0,
    padding: 0,
    top: 0,
    left: 0,
    border: "none",
    borderRadius: 0,
  },
};

const BookCard = (book: Book) => {
  const { imageUrl, title, authors, pageCount, publisher, published } = book;

  const bookImage = imageUrl || defaultBook;

  const [modal, setModal] = useState<boolean>(false);

  const openModal = () => {
    setModal(true);
  };

  const closeModal = () => {
    setModal(false);
  };

  return (
    <>
      <StyledBackgroundCard onClick={openModal}>
        <div>
          <img src={bookImage} alt="" />
        </div>
        <StyledInfosDiv>
          <StyledTitle>
            <p>{title}</p>
            <div>
              {authors.map((elm, ind) => (
                <p key={ind}>{elm}</p>
              ))}
            </div>
          </StyledTitle>
          <StyledSubInfos>
            <p>{pageCount} páginas</p>
            <p>Editora {publisher}</p>
            <p>publicado em {published}</p>
          </StyledSubInfos>
        </StyledInfosDiv>
      </StyledBackgroundCard>
      {modal && (
        <Modal
          ariaHideApp={false}
          isOpen={modal}
          onRequestClose={closeModal}
          style={customStyles}
        >
          <BookModal closeModal={closeModal} data={book} />
        </Modal>
      )}
    </>
  );
};

export default BookCard;
