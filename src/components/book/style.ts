import styled from "styled-components";

const StyledBackgroundCard = styled.div`
  width: 310px;
  height: 160px;

  background: #ffffff;
  box-shadow: 6px 6px 20px 5px rgb(84 16 95 / 13%);
  border-radius: 8px;

  margin: 16px;

  cursor: pointer;

  img {
    width: 81px;
    margin: 0px 16px;
  }

  display: flex;
  align-items: center;

  @media (min-width: 768px) {
    width: 270px;
  }
`;

const StyledInfosDiv = styled.div`
  height: 160px;

  display: flex;
  flex-direction: column;

  justify-content: space-around;
`;

const StyledTitle = styled.div`
  p {
    font-style: normal;
    font-weight: 500;
    font-size: 14px;
    line-height: 20px;

    color: #333333;
  }
  div {
    p {
      font-style: normal;
      font-weight: 400;
      font-size: 12px;
      line-height: 20px;

      color: #ab2680;
    }
  }
`;

const StyledSubInfos = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
  p {
    font-style: normal;
    font-weight: 400;
    font-size: 12px;
    line-height: 20px;

    color: #999999;
  }
`;

export { StyledBackgroundCard, StyledInfosDiv, StyledTitle, StyledSubInfos };
