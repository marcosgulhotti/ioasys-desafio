import { StyledErrorBox, StyledContainer, ArrowDiv } from "./style";

const FormError = () => {
  return (
    <StyledContainer>
      <ArrowDiv />
      <StyledErrorBox>
        <p>Email e/ou senha incorretos.</p>
      </StyledErrorBox>
    </StyledContainer>
  );
};

export default FormError;
