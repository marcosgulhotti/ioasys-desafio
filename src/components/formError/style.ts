import styled from "styled-components";

const StyledContainer = styled.div`
  flex-direction: column;

  animation-duration: 2s;
  animation-name: opacityAnimation;

  @keyframes opacityAnimation {
    from {
      opacity: 0;
    }
    to {
      opacity: 1;
    }
  }
`;

const StyledErrorBox = styled.div`
  width: 239px;
  height: 48px;
  background: rgba(255, 255, 255, 0.4);
  backdrop-filter: blur(2px);

  border-radius: 4px;

  margin: 0;

  display: flex;
  align-items: center;

  color: #ffffff;
  font-style: normal;
  font-weight: 700;
  font-size: 16px;
  line-height: 16px;

  justify-content: center;
`;

const ArrowDiv = styled.div`
  clip-path: polygon(50% 0%, 0% 100%, 100% 100%);

  width: 16px;
  height: 8px;

  margin-left: 15px;
  margin-bottom: -1px;

  background-color: rgba(255, 255, 255, 0.5);
`;

export { StyledErrorBox, StyledContainer, ArrowDiv };
