import { ModalProps } from "../../types";
import {
  StyledCloseDiv,
  StyledDescription,
  StyledImageDiv,
  StyledInfosBook,
  StyledInfosContainer,
  StyledInfosDiv,
  StyledModal,
  StyledPositionModal,
  StyledTitle,
} from "./style";
import defaultBook from "../../assets/books/defaultBook.png";

const BookModal = ({ data, closeModal }: ModalProps) => {
  const {
    imageUrl,
    title,
    authors,
    pageCount,
    publisher,
    published,
    language,
    isbn10,
    isbn13,
    description,
  } = data;

  const bookImage = imageUrl || defaultBook;

  return (
    <div>
      <StyledCloseDiv>
        <div onClick={closeModal}>
          <i className="fa-solid fa-xmark"></i>
        </div>
      </StyledCloseDiv>
      <StyledPositionModal>
        <StyledModal>
          <div>
            <StyledImageDiv src={bookImage} alt="Book" />
          </div>
          <StyledInfosDiv>
            <StyledTitle>
              <h1>{title}</h1>
              <p>{authors.join(", ")}</p>
            </StyledTitle>
            <StyledInfosContainer>
              <p className="infosHead">Informações</p>
              <StyledInfosBook>
                <p className="infoName">Páginas</p>
                <p className="info">{pageCount}</p>
              </StyledInfosBook>
              <StyledInfosBook>
                <p className="infoName">Editora</p>
                <p className="info">{publisher}</p>
              </StyledInfosBook>
              <StyledInfosBook>
                <p className="infoName">Publicação</p>
                <p className="info">{published}</p>
              </StyledInfosBook>
              <StyledInfosBook>
                <p className="infoName">Idioma</p>
                <p className="info">{language}</p>
              </StyledInfosBook>
              <StyledInfosBook>
                <p className="infoName">Titulo Original</p>
                <p className="info">{title}</p>
              </StyledInfosBook>
              <StyledInfosBook>
                <p className="infoName">ISBN-10</p>
                <p className="info">{isbn10}</p>
              </StyledInfosBook>
              <StyledInfosBook>
                <p className="infoName">ISBN-13</p>
                <p className="info">{isbn13}</p>
              </StyledInfosBook>
            </StyledInfosContainer>
            <StyledDescription>
              <h1>Resenha da editora</h1>
              <div>
                <p>
                  <i className="fa-solid fa-quote-left"></i> {description}
                </p>
              </div>
            </StyledDescription>
          </StyledInfosDiv>
        </StyledModal>
      </StyledPositionModal>
    </div>
  );
};

export default BookModal;
