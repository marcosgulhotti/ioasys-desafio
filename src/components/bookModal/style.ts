import styled from "styled-components";

const StyledCloseDiv = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-end;
  div {
    width: 32px;
    height: 32px;

    border: 1px solid rgba(51, 51, 51, 0.2);

    border-radius: 100%;

    display: flex;
    justify-content: center;
    align-items: center;

    background-color: #fff;

    margin-top: 20px;
    margin-right: 20px;

    cursor: pointer;

    i {
      color: #000;

      font-size: 16px;
    }
  }
`;

const StyledModal = styled.div`
  width: 770px;
  height: 600px;

  background-color: #ffffff;

  display: flex;

  border-radius: 5px;

  @media (max-width: 425px) {
    flex-direction: column;
    width: 280px;
    height: 945px;
  }

  @media (max-width: 768px) {
    flex-direction: column;
    width: 350px;
    height: 945px;
    align-items: center;
  }
`;

const StyledPositionModal = styled.div`
  display: flex;
  justify-content: center;

  margin-top: 50px;
  height: 100vh;

  border-radius: 5px;
  
  @media (max-width: 768px) {
    margin-top: 20px;
  }
`;

const StyledImageDiv = styled.img`
  margin: 48px;
  width: 350px;
  @media (max-width: 768px) {
    margin: 15px;
    width: 250px;
  }
`;

const StyledTitle = styled.div`
  max-width: 276px;
  h1 {
    font-style: normal;
    font-weight: 500;
    font-size: 28px;
    line-height: 40px;

    color: #333333;

    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  }
  p {
    font-style: normal;
    font-weight: 400;
    font-size: 12px;
    line-height: 20px;

    color: #ab2680;
  }
`;

const StyledInfosDiv = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  margin-right: 48px;

  @media (max-width: 768px) {
    margin: 15px;
  }
`;

const StyledInfosBook = styled.div`
  display: flex;
  justify-content: space-between;

  .info {
    font-style: normal;
    font-weight: 400;
    font-size: 12px;
    line-height: 20px;

    text-align: right;

    color: #999999;
  }

  .infoName {
    font-style: normal;
    font-weight: 500;
    font-size: 12px;
    line-height: 28px;

    color: #333333;
  }
`;

const StyledInfosContainer = styled.div`
  .infosHead {
    font-style: normal;
    font-weight: 500;
    font-size: 12px;
    line-height: 28px;

    text-transform: uppercase;

    color: #333333;
  }
`;

const StyledDescription = styled.div`
  h1 {
    font-style: normal;
    font-weight: 500;
    font-size: 12px;
    line-height: 20px;

    text-transform: uppercase;
  }
  div {
    margin-top: 15px;
    font-style: normal;
    font-weight: 400;
    font-size: 12px;
    line-height: 20px;
    color: #999999;
  }
`;

export {
  StyledCloseDiv,
  StyledModal,
  StyledPositionModal,
  StyledImageDiv,
  StyledTitle,
  StyledInfosDiv,
  StyledInfosBook,
  StyledInfosContainer,
  StyledDescription,
};
