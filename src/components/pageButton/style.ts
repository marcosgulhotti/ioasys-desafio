import styled from "styled-components";

const StyledContainer = styled.div`
  @media (min-width: 768px) {
    .Desktop {
      display: flex;

      align-items: center;

      p {
        margin-right: 16px;
        font-style: normal;
        font-weight: 400;
        font-size: 12px;
        line-height: 20px;

        color: #333333;
        span {
          font-weight: bold;
          font-weight: 500;
        }
      }
      button {
        width: 32px;
        height: 32px;

        border-radius: 100%;
        border: 1px solid rgba(51, 51, 51, 0.2);
        box-sizing: border-box;

        display: flex;
        justify-content: center;
        align-items: center;

        margin: 0px 4px;
        i {
          width: 4px;
        }
      }
    }
    .Mobile {
      display: none;
    }
  }
  @media (max-width: 1130px) {
    .Mobile {
      display: flex;

      align-items: center;

      p {
        margin: 0px 16px;
        font-style: normal;
        font-weight: 400;
        font-size: 12px;
        line-height: 20px;

        color: #333333;
        span {
          font-weight: bold;
          font-weight: 500;
        }
      }
      button {
        width: 32px;
        height: 32px;

        border-radius: 100%;
        border: 1px solid rgba(51, 51, 51, 0.2);
        box-sizing: border-box;

        display: flex;
        justify-content: center;
        align-items: center;

        margin: 0px 4px;
        i {
          width: 4px;
        }
      }
    }
    .Desktop {
      display: none;
    }
  }
`;

export { StyledContainer };
