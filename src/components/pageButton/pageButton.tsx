import { pagesFunctionProps } from "../../types";
import { StyledContainer } from "./style";

const PageButton = ({
  page,
  totalPages,
  nextPage,
  previousPage,
}: pagesFunctionProps) => {
  return (
    <>
      <StyledContainer>
        <div className="Desktop">
          <p>
            Página <span>{page}</span> de <span>{totalPages}</span>
          </p>
          {page === 1 ? (
            <button disabled onClick={() => previousPage(page)}>
              <i className="fa-solid fa-angle-left"></i>
            </button>
          ) : (
            <button onClick={() => previousPage(page)}>
              <i className="fa-solid fa-angle-left"></i>
            </button>
          )}

          {page === totalPages ? (
            <button disabled onClick={() => nextPage(page)}>
              <i className="fa-solid fa-angle-right"></i>
            </button>
          ) : (
            <button onClick={() => nextPage(page)}>
              <i className="fa-solid fa-angle-right"></i>
            </button>
          )}
        </div>
        <div className="Mobile">
          {page === 1 ? (
            <button disabled onClick={() => previousPage(page)}>
              <i className="fa-solid fa-angle-left"></i>
            </button>
          ) : (
            <button onClick={() => previousPage(page)}>
              <i className="fa-solid fa-angle-left"></i>
            </button>
          )}
          <p>
            Página <span>{page}</span> de <span>{totalPages}</span>
          </p>
          {page === totalPages ? (
            <button disabled onClick={() => nextPage(page)}>
              <i className="fa-solid fa-angle-right"></i>
            </button>
          ) : (
            <button onClick={() => nextPage(page)}>
              <i className="fa-solid fa-angle-right"></i>
            </button>
          )}
        </div>
      </StyledContainer>
    </>
  );
};

export default PageButton;
