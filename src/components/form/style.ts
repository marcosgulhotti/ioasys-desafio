import styled from "styled-components";

const StyledContainer = styled.div`
  margin-left: 0;
  @media (min-width: 580px) {
    margin-left: 5%;
  }
`;

const StyledLogoContainer = styled.div`
  width: 300px;
  @media (min-width: 580px) {
    width: 368px;
  }

  display: flex;
  flex-direction: row;

  margin-bottom: 50px;

  p {
    color: #ffffff;

    font-style: normal;
    font-weight: 300;
    font-size: 32px;

    line-height: 40px;

    margin-left: 16.6px;
  }
`;

const StyledLogo = styled.img``;

const StyledForm = styled.form`
  div {
    margin: 16px 0px;
  }
`;

export { StyledContainer, StyledLogoContainer, StyledLogo, StyledForm };
