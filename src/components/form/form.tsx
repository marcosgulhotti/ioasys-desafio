/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  StyledContainer,
  StyledLogoContainer,
  StyledLogo,
  StyledForm,
} from "./style";
import Logo from "../../assets/logo/Logo.svg";
import Input from "../input/input";
import { useNavigate } from "react-router";
import api from "../../services/api";
import * as yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { LoginInterfaceProps } from "../../types";
import FormError from "../formError/formError";
import { useState } from "react";

import { useAppDispatch, useAppSelector } from "../../store/hooks";
import { setUser, saveUserInLocalStorage } from "../../store/slices/userSlice";

const FormContainer = () => {
  const navigate = useNavigate();

  const dispatch = useAppDispatch();
  const { name } = useAppSelector((state) => state.userSlice);

  const [errorModal, setErrorModal] = useState(false);

  const schema = yup.object().shape({
    email: yup.string().required().email(),
    password: yup.string().required(),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm<LoginInterfaceProps>({
    resolver: yupResolver(schema),
  });

  const handleSubmitFunction = async (data: LoginInterfaceProps) => {
    try {
      const req = await api.post("/auth/sign-in", data);
      const { authorization } = req.headers;

      setErrorModal(false);

      dispatch(setUser(req.data));

      localStorage.setItem("token", JSON.stringify(authorization));

      dispatch(saveUserInLocalStorage(req.data));

      navigate("/dashboard");
    } catch {
      setErrorModal(true);
      reset();
    }
  };

  return (
    <StyledContainer>
      <StyledLogoContainer>
        <StyledLogo data-testid={"logo"} src={Logo} alt="ioasys logo" />
        <p>Books</p>
      </StyledLogoContainer>

      <StyledForm onSubmit={handleSubmit(handleSubmitFunction)}>
        <Input
          register={register("email")}
          placeholder="Digite seu email"
          label="Email"
          type="text"
        />
        <Input
          register={register("password")}
          placeholder="Digite sua senha"
          type="password"
          label="Senha"
          inputWhithButton
        />
      </StyledForm>
      {errorModal && <FormError />}
    </StyledContainer>
  );
};

export default FormContainer;
