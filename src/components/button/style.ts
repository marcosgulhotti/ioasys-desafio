import styled from "styled-components";

const StyledButton = styled.button`
  background: #ffffff;
  border-radius: 44px;
  border: none;

  width: 85px;
  height: 36px;

  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  line-height: 20px;

  color: #b22e6f;

`;

export { StyledButton };
