import { ButtonInterfaceProps } from "../../types";
import { StyledButton } from "./style";

const SubmitFormButton = ({ children, ...rest }: ButtonInterfaceProps) => {
  return (
    <StyledButton data-testid="login-button" {...rest}>
      {children}
    </StyledButton>
  );
};

export default SubmitFormButton;
