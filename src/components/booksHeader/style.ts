import styled from "styled-components";

const StyledLogo = styled.div`
  display: flex;
  align-items: center;

  p {
    font-style: normal;
    font-weight: 300;
    font-size: 28px;
    line-height: 40px;

    color: #333333;

    margin-left: 16px;
  }
  img {
    width: 7rem;
  }
`;

const StyledContainer = styled.div`
  max-width: 1330px;
  display: flex;
  justify-content: space-around;
  @media (min-width: 540px) {
    width: 520px;
    justify-content: space-between;
  }

  @media (min-width: 768px) {
    width: 640px;
    justify-content: space-between;
  }
  @media (min-width: 1024px) {
    width: 950px;
  }
  @media (min-width: 1200px) {
    width: 1100px;
  }

  @media (min-width: 1330px) {
    width: 1350px;
  }

  margin-top: 40px;
`;

const StyledHeaderDiv = styled.div`
  display: flex;
  align-items: center;

  .circleDiv {
    width: 32px;
    height: 32px;

    border-radius: 100%;
    border: 1px solid rgba(51, 51, 51, 0.2);
    box-sizing: border-box;

    display: flex;
    align-items: center;
    justify-content: center;

    margin-left: 16px;

    cursor: pointer;

    color: #333333;
  }

  p {
    display: none;
  }
  @media (min-width: 540px) {
    p {
      display: block;
      color: #333333;
      span {
        font-weight: bold;
      }
    }
  }
`;

export { StyledLogo, StyledContainer, StyledHeaderDiv };
