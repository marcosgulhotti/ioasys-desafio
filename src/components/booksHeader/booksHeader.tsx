import LogoBlack from "../../assets/logo/LogoBlack.svg";
import { useNavigate } from "react-router-dom";
import { useEffect } from "react";
import { StyledContainer, StyledHeaderDiv, StyledLogo } from "./style";
import { useAppSelector } from "../../store/hooks";

const BooksHeader = () => {
  const navigate = useNavigate();
  const nameUser = JSON.parse(localStorage.getItem("user") || "null");
  const token = JSON.parse(localStorage.getItem("token") || "null");

  const { name } = useAppSelector((state) => state.userSlice);

  useEffect(() => {
    if (token === null || nameUser === null) {
      navigate("/login");
    }
  }, [navigate, token, nameUser]);

  const userName = name || nameUser;

  const handleLogOut = () => {
    localStorage.clear();
    navigate("/login");
  };

  return (
    <StyledContainer>
      <StyledLogo>
        <img src={LogoBlack} alt="Black version of the company logo" />
        <p>Books</p>
      </StyledLogo>
      <StyledHeaderDiv>
        <p>
          Bem vindo, <span>{userName}!</span>
        </p>
        <div onClick={handleLogOut} className="circleDiv">
          <i className="fa-solid fa-arrow-right-from-bracket" />
        </div>
      </StyledHeaderDiv>
    </StyledContainer>
  );
};

export default BooksHeader;
