import styled, { css } from "styled-components";

const StyledInputContainer = styled.div<{ inputWhithButton: boolean }>`
  width: 300px;
  height: 60px;

  @media (min-width: 424px) {
    width: 368px;
    height: 60px;
  }

  background: rgba(0, 0, 0, 0.32);
  backdrop-filter: blur(2px);

  display: flex;
  justify-content: space-between;
  align-items: center;

  border-radius: 4px;

  padding: 10px;

  cursor: text;

  p {
    font-style: normal;
    font-weight: 400;
    font-size: 12px;
    line-height: 16px;

    color: #ffffff;

    opacity: 0.5;

    margin-left: 1px;
  }

  ${({ inputWhithButton }) =>
    inputWhithButton
      ? css`
          div {
            width: 75%;
          }
        `
      : css`
          div {
            width: 100%;
          }
        `}

  div {
    display: flex;
    flex-direction: column;
    margin: 0;

    box-sizing: border-box;

    padding-right: 10px;
  }
`;

const StyledInput = styled.input`
  background: none;
  border: none;

  color: #ffffff;
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  line-height: 24px;

  ::placeholder {
    color: #ffffff;
    opacity: 0.6;
  }
`;

export { StyledInputContainer, StyledInput };
