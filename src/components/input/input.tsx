import { useRef } from "react";
import { InputInterfaceProps } from "../../types";
import SubmitFormButton from "../button/SubmitFormButton";
import { StyledInputContainer, StyledInput } from "./style";

const Input = ({
  label,
  inputWhithButton,
  type,
  register,
  ...rest
}: InputInterfaceProps) => {
  const divElementRef = useRef<HTMLDivElement | null>(null);

  function onDivClick() {
    const element = divElementRef.current?.querySelector("input");

    if (element) element.focus();
  }

  return (
    <StyledInputContainer
      inputWhithButton={inputWhithButton ? true : false}
      onClick={onDivClick}
    >
      <div ref={divElementRef}>
        <p>{label}</p>
        <StyledInput {...register} type={type} {...rest} />
      </div>
      {inputWhithButton && <SubmitFormButton>Entrar</SubmitFormButton>}
    </StyledInputContainer>
  );
};

export default Input;
