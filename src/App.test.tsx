import { render, screen } from "@testing-library/react";
import App from "./App";

describe("login screen", () => {
  test("if input to login exists", () => {
    render(<App />);
    const inputEmail = screen.getByPlaceholderText("Digite seu email");
    const inputSenha = screen.getByPlaceholderText("Digite seu email");

    expect(inputEmail).not.toBeNull();
    expect(inputSenha).not.toBeNull();
  });
  test("if button is in screen", () => {
    render(<App />);
    const loginButton = screen.getByTestId("login-button");

    expect(loginButton).not.toBeNull();
  });
});

describe("dashboard screen", () => {
  test("if logo exists", () => {
    render(<App />);

    const logo = screen.getAllByTestId("logo");

    expect(logo).not.toBeNull();
  });
});
