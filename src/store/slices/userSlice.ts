import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { UserInterface } from "../types/User";
import { RootState } from "../index";

const initialState: UserInterface = {
  name: "",
  email: "",
  birthdate: "",
  gender: "",
  id: "",
};

export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    setUser: (state, action: PayloadAction<UserInterface>) => {
      state.name = action.payload.name;
      state.email = action.payload.email;
      state.birthdate = action.payload.birthdate;
      state.gender = action.payload.gender;
      state.id = action.payload.id;
    },
    saveUserInLocalStorage: (state, action: PayloadAction<any>) => {
      localStorage.setItem("user", JSON.stringify(state.name));
    },
  },
});

export const { setUser, saveUserInLocalStorage } = userSlice.actions;

export const selectUser = (state: RootState) => state.userSlice.name;

export default userSlice.reducer;
