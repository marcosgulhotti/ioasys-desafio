interface UserInterface {
  name: string;
  email: string;
  birthdate: string;
  gender: string;
  id: string;
}

export type { UserInterface };
