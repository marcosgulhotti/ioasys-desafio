import styled from "styled-components";
import loginBackground from "../../assets/BackgroundImages/loginBackground.png";

const StyledContainer = styled.div`
  @media (max-width: 425px) {
    background-attachment: fixed;
    background-position: 40%;
    
  }
  background: url(${loginBackground}) no-repeat center center fixed;
  min-height: 100vh;
  background-size: cover;

  display: flex;
  justify-content: center;
  align-items: center;

  @media (min-width: 580px) {
    justify-content: flex-start;
  }
`;

export { StyledContainer };
