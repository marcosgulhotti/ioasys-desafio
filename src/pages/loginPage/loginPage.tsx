import { StyledContainer } from "./style";
import FormContainer from "../../components/form/form";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";

const LoginPage = () => {
  const navigate = useNavigate();
  const token = JSON.parse(localStorage.getItem("token") || "null");

  useEffect(() => {
    if (token !== null) {
      navigate("/dashboard");
    }
  }, [navigate, token]);
  return (
    <StyledContainer>
      <FormContainer />
    </StyledContainer>
  );
};

export default LoginPage;
