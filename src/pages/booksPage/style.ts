import styled from "styled-components";
import booksBackground from "../../assets/BackgroundImages/booksBackground.png";

import secondBooksBackground from "../../assets/BackgroundImages/secondBooksBackground.png";

const StyledContainer = styled.div`
  background: url(${booksBackground}) no-repeat center center fixed;
  background-attachment: scroll;
  min-height: 100vh;
  background-size: cover;
  background-blend-mode: darken;

  display: flex;
  flex-direction: column;
  align-items: center;

  .positionHeader {
    display: flex;
    justify-content: center;
  }
`;

const StyledSecondBackground = styled.div`
  background: url(${secondBooksBackground}) no-repeat center center fixed;
  background-attachment: scroll;
  min-height: 100vh;
  background-size: cover;
`;

const StyledBooksContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  flex-direction: row;

  max-width: 1370px;
  max-height: 550px;

  margin: 50px;
  .displayBooks {
    display: flex;
    flex-wrap: wrap;
    flex-direction: row;
    align-items: center;
    justify-content: center;
  }
  @media (min-width: 1025px) {
    .displayBooks {
      display: flex;
      flex-direction: row;
      align-items: center;
      flex-wrap: wrap;
      width: 100%;
      margin-right: 30px;
      justify-content: flex-end;
    }
  }
`;

export { StyledContainer, StyledSecondBackground, StyledBooksContainer };
