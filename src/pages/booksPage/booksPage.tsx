import { useEffect, useState } from "react";
import BookCard from "../../components/book/book";
import BooksHeader from "../../components/booksHeader/booksHeader";
import api from "../../services/api";
import { Book } from "../../types";
import {
  StyledBooksContainer,
  StyledContainer,
  StyledSecondBackground,
} from "./style";
import { useNavigate } from "react-router-dom";
import PageButton from "../../components/pageButton/pageButton";

const BooksPage = () => {
  const navigate = useNavigate();
  const token = JSON.parse(localStorage.getItem("token") || "null");

  const [page, setPage] = useState<number>(1);
  const [totalPages, setTotalPages] = useState<number>(0);
  const [books, setBooks] = useState<Array<Book>>([]);

  const getBooks = async () => {
    const req = await api.get(`books?page=${page}&amount=12`, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Token: ${token}`,
      },
    });
    setBooks(req.data.data);
    let fixedTotalPages = Math.round(req.data.totalPages);
    setTotalPages(fixedTotalPages);
  };
  useEffect(() => {
    getBooks();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page]);

  if (books === undefined) {
    navigate("/login");
  }

  const handleNextPage = (page: number) => {
    if (page !== totalPages) {
      setPage(page + 1);
    }
  };

  const handlePreviousPage = (page: number) => {
    if (page > 1) {
      setPage(page - 1);
    }
  };

  return (
    <StyledSecondBackground>
      <StyledContainer>
        <div className="positionHeader">
          <BooksHeader />
        </div>
        <StyledBooksContainer>
          <div data-testid="books-data" className="displayBooks">
            {books.map((elmt, ind) => (
              <BookCard key={ind} {...elmt} />
            ))}
            <PageButton
              page={page}
              totalPages={totalPages}
              nextPage={handleNextPage}
              previousPage={handlePreviousPage}
            />
          </div>
        </StyledBooksContainer>
      </StyledContainer>
    </StyledSecondBackground>
  );
};

export default BooksPage;
