import { UseFormRegisterReturn } from "react-hook-form";

interface InputInterfaceProps {
  label: string;
  inputWhithButton?: boolean;
  type?: "text" | "password";
  placeholder?: string;
  register: UseFormRegisterReturn;
}

type ButtonInterfaceProps = {
  children: React.ReactChild;
};

interface LoginInterfaceProps {
  email: string;
  password: string;
}

interface Book {
  authors: Array<string>;
  title: string;
  description: string;
  pageCount: number;
  category: string;
  imageUrl: string;
  language: string;
  isbn10: string;
  isbn13: string;
  publisher: string;
  published: number;
  id: string;
}

interface ArrayBooks {
  data: Array<Book> | undefined;
  page: number | 0;
  totalItems: number | 0;
  totalPages: number | 0;
}

interface pagesFunctionProps {
  page: number;
  totalPages: number;
  nextPage: (page: number) => void;
  previousPage: (page: number) => void;
}

interface ModalProps {
  data: Book;
  closeModal: () => void;
}

export type {
  InputInterfaceProps,
  ButtonInterfaceProps,
  LoginInterfaceProps,
  Book,
  ArrayBooks,
  pagesFunctionProps,
  ModalProps,
};
